import pandas as pd
import os
import plotly.express as px
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image
import cv2
from scipy import stats
from skimage.metrics import structural_similarity as ssim
from skimage import img_as_float
from sklearn.preprocessing import normalize
from scipy.special import kl_div


video_time = {'Basketball':17070, 'Girl1':8700, 'Girl2':25030, 'ManRunning1':24770, 'ManRunning2':10400,
    'Soccer1': 24500, 'Soccer2': 9300, 'StreetBasketball1':9630, 'Walking':15800, 'bike2':22100, 'bike3':17300,
    'building1': 18770, 'building2':23070, 'building3':33170, 'building4':31470, 'car11':13470, 'car12':19970,
    'car13': 16600, 'car15':18770, 'person22': 7970, 'truck2':24030, 'truck3':21400
}

tailleDivision = 1 # 5 min, 200 max (plus c'est petit, plus c'est précis)
sigma = 60
timeInterval = 240
resolutionX = 1360
resolutionY = 768
nbXdivisions = resolutionX//tailleDivision + 1
nbYdivisions = resolutionY//tailleDivision + 1


# group 0 = le labo

# créé un fichier trié par timestamp pour une vidéo
def create_file_video(folder, video_name, group=0, verbose=True):
    # liste tout les fichiers result_bubble du dossier lab_query_params
    #dirPath = r"./BubbleViewUAV2/lab_query_params/"
    if group==0: # si on prend les résultats du labo
        result = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and "bubble" in f]
    else:
        result = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and f"gr{group}" in f]
    if verbose: print(f"{len(result)} participants")
    dfresult = None

    for file_name in result:
        df = pd.read_csv(folder+file_name)
        if group==0:
            dfresult = pd.concat([dfresult, df[df["stimuli"]==("diss_"+video_name+".mp4")]])
        else:
            dfresult = pd.concat([dfresult, df[df["videoName"]==video_name]])
    if "stimuli_id" in dfresult.columns:
        dfresult.drop(["stimuli_id"], axis=1, inplace=True)
    dfresult.columns = ["timestamp","mouseX","mouseY","videoName"]
    dfresult = dfresult.sort_values(by="timestamp")
    dfresult.to_csv(f"{video_name}_Gr{group}.csv", index=False)
    if verbose: print("video file created")


# DFresult(temps,x,y,size) avec tout les points qui ne valent pas 0
def create_matrixes(video_name, group=0):
    saliency = np.zeros((nbYdivisions,nbXdivisions))

    df = pd.read_csv(f"{video_name}_Gr{group}.csv")

    DFresult = pd.DataFrame(columns=["time","X","Y","size"])

    start = 0
    while(True):
        m = create_one_matrix(df,start,start+timeInterval)
        if start>video_time[video_name]:
            break
        for x, y in zip(m.nonzero()[0], m.nonzero()[1]):
            DFresult = pd.concat([DFresult, pd.DataFrame({"time":[start], "X":[y], "Y":[x], "size":[m[x,y]]})], ignore_index=True)
        start += timeInterval
    return DFresult
        

# créé une matrice d'attention dans un intervalle de temps en millisecondes
def create_one_matrix(df,start,end):
    saliency = np.zeros((nbYdivisions,nbXdivisions))

    df2 = df[start<=df["timestamp"]]
    df2 = df2[df2["timestamp"]<end]

    if len(df2)>0:
        old_index = df2.index[0]
        for i in df2.index:
            if old_index+1 == i: # c'est qu'on est toujours sur la même personne
                saliency[int(df2["mouseY"][i])//tailleDivision,int(df2["mouseX"][i])//tailleDivision] += abs((df2["timestamp"][i]-df2["timestamp"][old_index]))
            elif i>0: # changement de personne et pas au début donc va va cherche le timestamp d'avant qui est dans df
                saliency[int(df2["mouseY"][i])//tailleDivision,int(df2["mouseX"][i])//tailleDivision] += abs((df2["timestamp"][i]-df["timestamp"][i-1]))
            old_index = i

    return saliency



def create_file_result(video_name, group_number, verbose=True): # créé une fichier de résultat à partir d'une vidéo
    DFmatrixes = create_matrixes(f"{video_name}", group_number)
    DFmatrixes.to_csv(f"result_{video_name}_Gr{group_number}.csv",index=False)
    if verbose: print("result file created")


def do_all(folder, file, group_number=0, verbose=True):
    create_file_video(folder, file, group=group_number, verbose=verbose)

    # créé le fichier de résultat
    create_file_result(file, group_number, verbose=verbose)


def complete_one_frame(df1, df2, time):
    df1 = df1[df1["time"]==time]
    df2 = df2[df2["time"]==time]

    for i in range(nbXdivisions):
        dfx1 = df1[df1['X'] == i]
        dfx2 = df2[df2['X'] == i]
        for j in range(nbYdivisions):
            dfxy = dfx1[dfx1['Y'] == j]
            if len(dfxy)==0: # si il existe pas
                df1 = pd.concat([df1, pd.DataFrame({"time":[time], "X":[i], "Y":[j], "size":[0]})], ignore_index=True)
            dfxy = dfx2[dfx2['Y'] == j]
            if len(dfxy)==0: # si il existe pas
                df2 = pd.concat([df2, pd.DataFrame({"time":[time], "X":[i], "Y":[j], "size":[0]})], ignore_index=True)

    return ( df1.sort_values(by=["X",'Y'])['size'], df2.sort_values(by=["X",'Y'])['size'] )


def measure(video_name):
    df1 = pd.read_csv(f"result_{video_name}_Gr1.csv")
    df2 = pd.read_csv(f"result_{video_name}_Gr2.csv")

    results_cc = []
    results_sim = []

    for t in range(0, video_time[video_name], timeInterval):
        one_frame_res = complete_one_frame(df1, df2, t)
        
        # calcul de la corrélation
        res_cc = stats.pearsonr(one_frame_res[0], one_frame_res[1])
        results_cc.append(res_cc[0])

        # calcul de la similarité structurel
        img_1 = img_as_float(one_frame_res[0].to_numpy())
        img_2 = img_as_float(one_frame_res[1].to_numpy())
        results_sim.append(ssim(img_1, img_2, data_range=img_2.max() - img_2.min()))

    print(f"{video_name} & {round(np.mean(results_cc),3)} & {round(np.mean(results_sim),3)} \\\\")
    return (np.mean(results_cc), np.mean(results_sim))


def do_measure_all_videos():
    print(f"{len([f for f in os.listdir('./result_godot_expe/') if os.path.isfile(os.path.join('./result_godot_expe/', f))])} participants")
    pcc_dict = {}
    sim_dict = {}
    for v in video_time.keys(): # enlever les commentaires si changements dans les participants
        #do_all("./result_godot_expe/", v, group_number=1, verbose=False)
        #do_all("./result_godot_expe/", v, group_number=2, verbose=False)
        results = measure(v)
        pcc_dict[v] = results[0]
        sim_dict[v] = results[1]
    print("---sort by PCC---")
    sorted_pcc_dict = dict(sorted(pcc_dict.items(), key=lambda x:x[1]))
    for i, (k, v) in enumerate(sorted_pcc_dict.items()):
        print(f"{i+1} & {k} & {round(v,3)} & {round(sim_dict[k],3)} \\\\")
    print(f"moyenne & {round(np.mean(list(sorted_pcc_dict.values())),3)} & {round(np.mean(list(sim_dict.values())),3)} \\\\")


def plot_saliency(file, group_number=1):
    # créé le graphique interactif à partir du fichier de résultat
    DFmatrixes = pd.read_csv(f"result_{file}_Gr{group_number}.csv")

    for t in range(0, video_time[file], timeInterval): # pour que la heatmap remplisse toute la carte
        DFmatrixes = pd.concat([DFmatrixes, pd.DataFrame({"time":[t], "X":[0], "Y":[0], "size":[0]})], ignore_index=True)
        DFmatrixes = pd.concat([DFmatrixes, pd.DataFrame({"time":[t], "X":[nbXdivisions], "Y":[nbYdivisions], "size":[0]})], ignore_index=True)


    fig = px.density_heatmap(DFmatrixes, x="X", y="Y", z="size", animation_frame="time", nbinsx=nbXdivisions*2, nbinsy=nbYdivisions*2, range_x=[-0.5,nbXdivisions+0.5], range_y=[nbYdivisions+0.5,-0.5], range_color=[0,50])
    fig.show()




#do_all("./result_godot_expe/", "building1", group_number=1)
#do_all("./result_godot_expe/", "building1", group_number=2)
#measure("building1")
#do_measure_all_videos()
#plot_saliency("ManRunning2")


def df_to_matrix(df, normed=False):
    m = np.zeros((resolutionY,resolutionX))
    for index, row in df.iterrows(): 
        m[int(row["Y"])][int(row["X"])] = int(row["size"])
    if normed:
        return normalize(m)
    else:
        return m


def save_blurred_image_one_frame(video_name, time):
    do_all("./result_godot_expe/", video_name, group_number=1)
    df = pd.read_csv(f"result_{video_name}_Gr1.csv")
    src = df_to_matrix(df[df['time']==time])
    dst = cv2.GaussianBlur(src,(0,0), sigmaX=sigma)
    matplotlib.image.imsave('blurred1.png', dst)

    
    do_all("./result_godot_expe/", video_name, group_number=2)
    df = pd.read_csv(f"result_{video_name}_Gr2.csv")
    src = df_to_matrix(df[df['time']==time])
    dst = cv2.GaussianBlur(src,(0,0), sigmaX=sigma)
    matplotlib.image.imsave('blurred2.png', dst)

save_blurred_image_one_frame("truck2", 6000)

def corr_intravideo(video_name):
    pass



def measures_intergroup_one_video(video_name):
    do_all("./result_godot_expe/", video_name, group_number=1, verbose=False)
    do_all("./result_godot_expe/", video_name, group_number=2, verbose=False)

    df1 = pd.read_csv(f"result_{video_name}_Gr1.csv")
    df2 = pd.read_csv(f"result_{video_name}_Gr2.csv")

    results_cc = []
    results_sim = []

    for t in range(0, video_time[video_name], timeInterval):
        matrix1 = df_to_matrix(df1[df1["time"]==t], normed=True)
        matrix2 = df_to_matrix(df2[df2["time"]==t], normed=True)
        gaussed_matrix1 = cv2.GaussianBlur(matrix1,(0,0), sigmaX=sigma)
        gaussed_matrix2 = cv2.GaussianBlur(matrix2,(0,0), sigmaX=sigma)
        array1 = gaussed_matrix1.flatten()
        array2 = gaussed_matrix2.flatten()

        # calcul de la corrélation
        res_cc = stats.pearsonr(array1, array2)
        results_cc.append(res_cc[0])

        # calcul de la similarité structurel
        results_sim.append(ssim(gaussed_matrix1, gaussed_matrix2, data_range=gaussed_matrix2.max()-gaussed_matrix2.min()))

    print(f"{video_name} & {round(np.mean(results_cc),3)} & {round(np.mean(results_sim),3)} \\\\")
    return (np.mean(results_cc), np.mean(results_sim))

#measures_intergroup_one_video("ManRunning2")

def measure_all_videos():
    print(f"{len([f for f in os.listdir('./result_godot_expe/') if os.path.isfile(os.path.join('./result_godot_expe/', f))])} participants")
    pcc_dict = {}
    sim_dict = {}
    for v in video_time.keys(): # enlever les commentaires si changements dans les participants
        results = measures_intergroup_one_video(v)
        pcc_dict[v] = results[0]
        sim_dict[v] = results[1]
    print("---sort by PCC---")
    sorted_pcc_dict = dict(sorted(pcc_dict.items(), key=lambda x:x[1]))
    for i, (k, v) in enumerate(sorted_pcc_dict.items()):
        print(f"{i+1} & {k} & {round(v,3)} & {round(sim_dict[k],3)} \\\\")
    print(f"moyenne & {round(np.mean(list(sorted_pcc_dict.values())),3)} & {round(np.mean(list(sim_dict.values())),3)} \\\\")
    print(list(sorted_pcc_dict.values()))

#measure_all_videos()

#results = [0.49538498420269045, 0.5204703723175812, 0.5234272520743852, 0.5275616356747034, 0.5347844382494846, 0.5424605087692411, 0.5611746528848667, 0.5628587362350658, 0.5705684206254027, 0.5783047974081983, 0.5845582559937066, 0.5983203785195639, 0.598802894084736, 0.6092726928937423, 0.614064395983012, 0.6188583881303173, 0.6776669794981989, 0.6905601229858804, 0.6906852713386555, 0.7034974127014689, 0.7704239317762647, 0.8233372688993477]
#plt.barh(range(0,22), results, tick_label=['building1', 'car15', 'building4', 'truck3', 'bike2','car11', 'truck2', 'car13', 'building2', 'building3', 'Soccer1','bike3', 'person22', 'Basketball', 'car12', 'Girl1', 'Girl2', 'StreetBasketball1', 'ManRunning1', 'Walking', 'Soccer2', 'ManRunning2'])
#plt.show()

#----------------------------------------------------------------------------------------------------------
# pour comparer avec l'eye-tracker
def measures_eyetracker_one_video(video_name):
    do_all("./result_godot_expe/", video_name, group_number=1, verbose=False)
    do_all("./result_godot_expe/", video_name, group_number=2, verbose=False)

    df1 = pd.read_csv(f"result_{video_name}_Gr1.csv")
    df2 = pd.read_csv(f"result_{video_name}_Gr2.csv")

    results_cc_gr1 = []
    results_cc_gr2 = []

    eye_tracker_files = sorted([f for f in os.listdir(f"./heatmaps_eye_tracking_dissocie/HEATMAPS/{video_name}/{video_name}/Binocular")])

    for t in range(0, video_time[video_name], timeInterval):
        matrix1 = df_to_matrix(df1[df1["time"]==t], normed=True)
        matrix2 = df_to_matrix(df2[df2["time"]==t], normed=True)
        gaussed_matrix1 = cv2.GaussianBlur(matrix1,(0,0), sigmaX=sigma)
        gaussed_matrix2 = cv2.GaussianBlur(matrix2,(0,0), sigmaX=sigma)
        array1 = gaussed_matrix1.flatten()
        array2 = gaussed_matrix2.flatten()
        eye_tracker_im = cv2.imread(f"./heatmaps_eye_tracking_dissocie/HEATMAPS/{video_name}/{video_name}/Binocular/{eye_tracker_files[t//40]}")

        # Redimensionner la matrice pour obtenir des dimensions (768, 1360)
        matrice_transformee = np.zeros((768, 1360), dtype=np.uint8)
        for i in range(720):
            for j in range(1280):
                # Calculer la moyenne des trois valeurs de la matrice originale correspondant à chaque pixel de la matrice transformée
                moyenne_pixel = np.mean(eye_tracker_im[i][j])
                matrice_transformee[i, j] = int(moyenne_pixel)

        matrice_transformee = matrice_transformee.flatten()

        # calcul de la corrélation
        res_cc = stats.pearsonr(matrice_transformee, array1)
        results_cc_gr1.append(res_cc[0])
        res_cc = stats.pearsonr(matrice_transformee, array2)
        results_cc_gr2.append(res_cc[0])

    print(f"{video_name} & {round(np.mean(results_cc_gr1),3)} & {round(np.mean(results_cc_gr2),3)} \\\\")
    return (np.mean(results_cc_gr1), np.mean(results_cc_gr2))

def measure_eyetracker_all_videos():
    print(video_time.keys())
    print(f"{len([f for f in os.listdir('./result_godot_expe/') if os.path.isfile(os.path.join('./result_godot_expe/', f))])} participants")
    pcc_dict_gr1 = {}
    pcc_dict_gr2 = {}
    for v in video_time.keys(): # enlever les commentaires si changements dans les participants
        results = measures_eyetracker_one_video(v)
        pcc_dict_gr1[v] = results[0]
        pcc_dict_gr2[v] = results[1]
    print("Pour le groupe 1:")
    print(pcc_dict_gr1)
    print("Pour le groupe 2:")
    print(pcc_dict_gr2)


#measures_eyetracker_one_video("ManRunning2")
#measure_eyetracker_all_videos()
#for v in ['Basketball', 'Girl1', 'Girl2', 'ManRunning1', 'ManRunning2', 'Soccer1', 'Soccer2', 'StreetBasketball1', 'Walking', 'bike2', 'bike3', 'building1', 'building2', 'building3', 'building4', 'car11', 'car12', 'car13', 'car15', 'person22', 'truck2', 'truck3']:
#    measures_eyetracker_one_video(v)

normal_blur_group = {'Basketball': 2, 'Girl1': 1, 'Girl2': 2, 'ManRunning1': 1, 'ManRunning2': 2, 'Soccer1': 1, 'Soccer2': 2, 'StreetBasketball1': 2, 'Walking': 1, 'bike2': 1, 'bike3': 2, 'building1': 2, 'building2': 1, 'building3': 1, 'building4': 1, 'car11': 2, 'car12': 1, 'car13': 2, 'car15': 1, 'person22': 2, 'truck2': 2, 'truck3': 1}

r1 = {'Basketball': 0.40939485043623575, 'Girl1': 0.24278854110563702, 'Girl2': 0.46067958276917015, 'ManRunning1': 0.40368492113240073, 'ManRunning2': 0.5029142617302713, 'Soccer1': 0.4103793330620213, 'Soccer2': 0.5794419170009222, 'StreetBasketball1': 0.3540723444564018, 'Walking': 0.5694566284221433, 'bike2': 0.25614703064157207, 'bike3': 0.3662141816764448, 'building1': 0.2506269286822919, 'building2': 0.3435064599321848, 'building3': 0.31387973239200073, 'building4': 0.3045188106399369, 'car11': 0.29032154466252735, 'car12': 0.44329193344111484, 'car13': 0.34560104905510175, 'car15': 0.2727701558689334, 'person22': 0.3015856258627993, 'truck2': 0.26183020590741696, 'truck3': 0.2538630402909321}
r2 = {'Basketball': 0.46046955780619686, 'Girl1': 0.3362366085275586, 'Girl2': 0.48423524819586095, 'ManRunning1': 0.45516438888388744, 'ManRunning2': 0.5399863603296652, 'Soccer1': 0.4763746251370132, 'Soccer2': 0.6081634663135503, 'StreetBasketball1': 0.45991419488699536, 'Walking': 0.5567293671849346, 'bike2': 0.2934227122638451, 'bike3': 0.38807679498442726, 'building1': 0.3035903652121054, 'building2': 0.3512190091202921, 'building3': 0.30564280026754725, 'building4': 0.26706239914049784, 'car11': 0.34309787490512933, 'car12': 0.37762211964889447, 'car13': 0.30235417146017496, 'car15': 0.28683272600450127, 'person22': 0.20780288202244634, 'truck2': 0.20896711436954607, 'truck3': 0.27343418813904624}
sorted_r1 = dict(sorted(r1.items(), key=lambda x:x[1]))

l1 = []
l2 = []

for i, (k, v) in enumerate(sorted_r1.items()):
    if normal_blur_group[k] == 1:
        print(f"{i+1} & {k} & {round(r1[k],3)} & {round(r2[k],3)} \\\\")
        l1.append(round(r1[k],3))
        l2.append(round(r2[k],3))
    else:
        print(f"{i+1} & {k} & {round(r2[k],3)} & {round(r1[k],3)} \\\\")
        l1.append(round(r2[k],3))
        l2.append(round(r1[k],3))

print(f"& moyenne & {np.mean(l1)} & {np.mean(l2)} & &")

width = 0.4
ind = np.arange(len(sorted_r1))

fig, ax = plt.subplots()
ax.barh(ind, l1, width, color='dodgerblue', label='Méthode classique', tick_label=list(sorted_r1.keys()))
ax.barh(ind + width, l2, width, color='orange', label='Flou adaptatif')

#ax.set(yticklabels=list(sorted_r1.keys()))
ax.legend()

plt.show()