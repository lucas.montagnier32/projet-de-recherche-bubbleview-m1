import pandas as pd
import os
import plotly.express as px
import numpy as np
import matplotlib.pyplot as plt

video_time = {'Basketball':17070, 'Girl1':8700, 'Girl2':25030, 'ManRunning1':24770, 'ManRunning2':10400,
    'Soccer1': 24500, 'Soccer2': 9300, 'StreetBasketball1':9630, 'Walking':15800, 'bike2':22100, 'bike3':17300,
    'building1': 18770, 'building2':23070, 'building3':33170, 'building4':31470, 'car11':13470, 'car12':19970,
    'car13': 16600, 'car15':18770, 'person22': 7970, 'truck2':24030, 'truck3':21400
}

# créé un fichier trié par timestamp pour une vidéo
def create_file_video(folder, video_name, group=1):
    # liste tout les fichiers result_bubble du dossier lab_query_params
    #dirPath = r"./BubbleViewUAV2/lab_query_params/"
    #result = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and "bubble" in f]
    result = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and f"gr{group}" in f]
    print(f"{len(result)} participants")
    dfresult = None
    for file_name in result:
        df = pd.read_csv(folder+file_name)
        dfresult = pd.concat([dfresult, df[df["videoName"]==video_name]])
    # on les tri comme ça on fait juste un +1 a chaque fois que quelqu'un a regarder dans la zone a ce temps donnée
    dfresult = dfresult.sort_values(by="timestamp")
    dfresult.to_csv(f"{video_name}.csv", index=False)
    print("video file created")

tailleDivision = 50 # 5 min, 200 max (plus c'est petit, plus c'est précis)
resolutionX = 1280
resolutionY = 720
nbXdivisions = resolutionX//tailleDivision + 3
nbYdivisions = resolutionY//tailleDivision + 3

# DFresult(temps,x,y,size) avec tout les points qui ne valent pas 0
def create_matrixes(video_name):
    saliency = np.zeros((nbYdivisions,nbXdivisions))

    df = pd.read_csv(f"{video_name}.csv")

    DFresult = pd.DataFrame(columns=["time","X","Y","size"])

    start = 0
    while(True):
        m = create_one_matrix(df,start,start+30)
        if start>video_time[video_name]:
            break
        for x, y in zip(m.nonzero()[0], m.nonzero()[1]):
            DFresult = pd.concat([DFresult, pd.DataFrame({"time":[start], "X":[y], "Y":[x], "size":[m[x,y]]})], ignore_index=True)
        start += 30
    return DFresult
        

# créé une matrice d'attention dans un intervalle de temps en millisecondes
def create_one_matrix(df,start,end):
    saliency = np.zeros((nbYdivisions,nbXdivisions))

    df2 = df[start<=df["timestamp"]]
    df2 = df2[df2["timestamp"]<end]

    for i in df2.index:
        saliency[int(df2["mouseY"][i])//tailleDivision,int(df2["mouseX"][i])//tailleDivision] += 1
        old_index = i

    return saliency


def plot_matrix(matrix): # code non utilisé
    # pointsX et pointsY servent à faire la grille de chaque points
    pointsX = [ i + 0.5 for j in range(nbYdivisions) for i in range(nbXdivisions) ]
    pointsY = [ i + 0.5 for i in range(nbYdivisions) for j in range(nbXdivisions) ]
    importance = np.asarray(matrix).ravel() # diviser par x pour pas avoir des trop gros rond sur le graphique
    plt.scatter(x=pointsX, y=pointsY, s=importance, c=importance, marker='s')
    plt.axis([0, nbXdivisions, nbYdivisions, 0]) # pour inverser l'axe y
    plt.show()


def create_file_result(video_name): # créé une fichier de résultat à partir d'une vidéo
    DFmatrixes = create_matrixes(f"{video_name}")
    DFmatrixes.to_csv(f"result_{video_name}.csv",index=False)
    print("result file created")


def do_all(folder, file, group=1):
    create_file_video(folder, file, group=1)

    # créé le fichier de résultat
    create_file_result(file)

    # créé le graphique interactif à partir du fichier de résultat
    DFmatrixes = pd.read_csv(f"result_{file}.csv")
    DFmatrixes["size"] = np.log(DFmatrixes["size"])

    fig = px.scatter(DFmatrixes, x="X", y="Y", animation_frame="time", size="size", color="size", range_x=[0,nbXdivisions], range_y=[nbYdivisions,0])

    #fig["layout"].pop("updatemenus") # optional, drop animation buttons
    fig.show()


do_all("./result_godot_expe/", "bike3", group=2)