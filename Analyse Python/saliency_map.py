# vidéo de taille 1280x720
# une frame toute les 33,333333 millisecondes
# faire un fichier de données par vidéo pour tout les observateurs triés par timestamp
# faire un plot interactif

import csv
import numpy as np
import matplotlib.pyplot as plt

tailleDivision = 5 # 5 min, 200 max (plus c'est petit, plus c'est précis)
resolutionX = 1280
resolutionY = 720

# scanpath
def lab_analyse_bubble(observer, video):
    with open(f"./BubbleViewUAV2/lab_query_params/{observer}_results_bubble.csv", newline='') as csvfile:
        reader = csv.reader(csvfile)
        row = next(reader) # les entetes de colonnes

        nbXDivisions = resolutionX//tailleDivision
        nbYDivisions = resolutionY//tailleDivision
        saliency = np.zeros((nbYDivisions,nbXDivisions))
        print(saliency.shape)

        # calcul de la map d'importance
        old_timestamp = 0
        x_max = 0
        y_max = 0
        for row in reader:
            if int(row[2]) > x_max:
                x_max = int(row[2])
            if int(row[3]) > y_max:
                y_max = int(row[3])
            if row[4] != video:
                continue
            else: # ajout du temps passé sur une case de la grille
                saliency[int(row[3])//tailleDivision,int(row[2])//tailleDivision] += (int(row[0]) - old_timestamp)
                old_timestamp = int(row[0])

        # pointsX et pointsY servent à faire la grille de chaque points
        pointsX = [ i for j in range(nbYDivisions) for i in range(nbXDivisions) ]
        pointsY = [ i for i in range(nbYDivisions) for j in range(nbXDivisions) ]
        importance = np.asarray(saliency).ravel()
        plt.scatter(x=pointsX, y=pointsY, s=importance, c=importance)
        plt.axis([min(pointsX), max(pointsX), max(pointsY), min(pointsY)]) # pour inverser l'axe y
        plt.show()

        print(f"{x_max=}\n{y_max=}")

lab_analyse_bubble("observer36946_445","diss_Soccer1.mp4")

import os

def analyse_video(video_name):
    # liste tout les fichiers result_bubble du dossier lab_query_params
    dirPath = r"./BubbleViewUAV2/lab_query_params/"
    result = [f for f in os.listdir(dirPath) if os.path.isfile(os.path.join(dirPath, f)) and "bubble" in f]

    # init
    nbXdivisions = resolutionX//tailleDivision + 1
    nbYdivisions = resolutionY//tailleDivision + 1
    saliency = np.zeros((nbYdivisions,nbXdivisions))

    for file_name in result:
        with open(dirPath+file_name, newline='') as csvfile:
            reader = csv.reader(csvfile)
            row = next(reader) # les entetes de colonnes

            # calcul de la map d'importance
            old_timestamp = 0
            for row in reader:
                if row[4] != video_name:
                    break
                else: # ajout du temps passé sur une case de la grille
                    if int(row[0])==0: # si replay de la même vidéo
                        old_timestamp = 0
                    saliency[int(row[3])//tailleDivision,int(row[2])//tailleDivision] += (int(row[0]) - old_timestamp)
                    old_timestamp = int(row[0])

    print("print graph")
    # pointsX et pointsY servent à faire la grille de chaque points
    pointsX = [ i + 0.5 for j in range(nbYdivisions) for i in range(nbXdivisions) ]
    pointsY = [ i + 0.5 for i in range(nbYdivisions) for j in range(nbXdivisions) ]
    importance = np.asarray(saliency).ravel() # diviser par x pour pas avoir des trop gros rond sur le graphique
    plt.scatter(x=pointsX, y=pointsY, s=importance/nbXdivisions, c=importance, marker='s')
    plt.axis([0, nbXdivisions, nbYdivisions, 0]) # pour inverser l'axe y
    plt.show()

#analyse_video("diss_ManRunning1.mp4")
