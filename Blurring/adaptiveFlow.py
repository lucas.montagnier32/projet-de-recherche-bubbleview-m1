import numpy as np
import cv2 as cv
import os as os


cap = cv.VideoCapture(cv.samples.findFile("Blurring/korea-7506.mp4"))
ret, frame1 = cap.read()
#frame1 is a matrix of one canal. Each matrix cell contains three values (RGB values of the image)

#flow1 is the optical flow matrix of the first frame of the korea-7506 video. It is in a CV_32FC2 format. We convert it into a CV_32FC1 format
flow1 = np.load('Blurring/data/farneback_output/flow_frame_1.npy')
#print(flow1[1][1])
#We split the flow matrix in two separate one canal matrix
flow1Canal1, flow1Canal2, flow1Canal3 = cv.split(flow1)
#print(flow1Canal1[1][1])
#print(flow1Canal2[1][1])

#print(type(flow1Canal1))

flow1OneCanal = flow1Canal1 + flow1Canal2 + flow1Canal3
flow1OneCanal = ((flow1OneCanal - np.amin(flow1OneCanal) ) / (np.amax(flow1OneCanal) - np.amin(flow1OneCanal)))

#flow1 = (flow1 - np.amin(flow1)) / (np.amax(flow1) - np.amin(flow1))
#print(flow1OneCanal)
flowCopy1 = np.copy(flow1OneCanal)
flow1ThreeCanal = cv.merge([flow1OneCanal, flowCopy1, flowCopy1])

#print(flow1ThreeCanal)
#Copier la matrice flow1OneCanal trois fois pour en faire une matrice à 3 canaux, pareil pour oneMinusFlow, puis faire les calculs.

unblurredMap = np.multiply(flow1ThreeCanal, frame1)
unblurredMap = ((unblurredMap - np.amin(unblurredMap)) / (np.amax(unblurredMap) - np.amin(unblurredMap)))

blur_frame1 = cv.GaussianBlur(frame1, (0,0), sigmaX = 12)
oneMinusFlow = 1 - flow1ThreeCanal

blurredMap = np.multiply(blur_frame1, oneMinusFlow)
blurredMap = (blurredMap - np.amin(blurredMap)) / (np.amax(blurredMap) - np.amin(blurredMap)) *0.7
finalImage = unblurredMap + blurredMap
#print(np.amin(blurredMap))


#print(finalImage)
#Permet d'afficher l'image à partir d'une matrice à un seul canal
cv.imshow('Feur', finalImage)
cv.waitKey(0)
cv.destroyAllWindows()