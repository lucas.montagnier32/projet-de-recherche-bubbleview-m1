import numpy as np
import cv2 as cv
import os as os

#Video is opened and we get the flow frame for the video
load_path = '/home/tristan/CoursUniv/M1ALMA/TER/Original_Frames_Selected_Databases/UAV123_Only_Selected_Videos_Frames/car13'
list_files = os.listdir(load_path)
list_files.sort()
print('Bien commencé chef o7 !')
frame1 = cv.imread(load_path + '/' + list_files[0])
prvs = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[..., 1] = 255

num_frame_to_consider = len(list_files)
iFrame = 1
#We apply the flow for the chosen number of frame to be considered
while(iFrame < num_frame_to_consider):
    frame2 = cv.imread(load_path + '/' + list_files[iFrame])
    next = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(prvs, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    save_path='/home/tristan/CoursUniv/M1ALMA/TER/flowFrames/car13/flow_frame_%d.npy' % iFrame
    if not os.path.isdir(os.path.dirname(save_path)):
            os.makedirs(os.path.dirname(save_path))
    mag, ang = cv.cartToPolar(flow[..., 0], flow[..., 1])
    hsv[..., 0] = ang*180/np.pi/2
    hsv[..., 2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)
    bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
    np.save(save_path, bgr)
    prvs = next
    iFrame += 1

#We calculate for the video for each frame'''
load_path_flow = '/home/tristan/CoursUniv/M1ALMA/TER/flowFrames/car13'
list_flow_frames = os.listdir(load_path_flow)
list_flow_frames.sort()
iFrame = 0

range_min_val = 0
range_max_val = 0.08

while(iFrame < num_frame_to_consider - 1):
    frame = cv.imread(load_path + '/' + list_files[iFrame])
    #frame1 is a matrix of one canal. Each matrix cell contains three values (RGB values of the image)

    #flow1 is the optical flow matrix of the first frame of the korea-7506 video. It is in a CV_32FC2 format. We convert it into a CV_32FC1 format
    flow = np.load(load_path_flow + '/' + list_flow_frames[iFrame])
    #print(flow1[1][1])
    #We split the flow matrix in two separate one canal matrix
    flowCanal1, flowCanal2, flowCanal3 = cv.split(flow)
    #print(flow1Canal1[1][1])
    #print(flow1Canal2[1][1])

    #print(type(flow1Canal1))

    flowOneCanal = (flowCanal1 + flowCanal2 + flowCanal3)
    if (np.amin(flowOneCanal) == 0) and (np.amax(flowOneCanal) == 0):
         flowOneCanal = flowOneCanal
    else:
        flowOneCanal = (((flowOneCanal - np.amin(flowOneCanal) ) / (np.amax(flowOneCanal) - np.amin(flowOneCanal))) * (range_max_val-range_min_val)) + range_min_val
    #flow1 = (flow1 - np.amin(flow1)) / (np.amax(flow1) - np.amin(flow1))
    flowCopy1 = np.copy(flowOneCanal)
    flowThreeCanal = cv.merge([flowOneCanal, flowCopy1, flowCopy1])

    #print(flow1ThreeCanal)
    #Copier la matrice flow1OneCanal trois fois pour en faire une matrice à 3 canaux, pareil pour oneMinusFlow, puis faire les calculs.

    unblurredMap = np.multiply(flowThreeCanal, frame)
    '''
    if (np.amin(unblurredMap) == 0) and (np.amax(unblurredMap) == 0):
         unblurredMap = unblurredMap
    else:
        unblurredMap = ((unblurredMap - np.amin(unblurredMap)) / (np.amax(unblurredMap) - np.amin(unblurredMap)))
    '''
    blur_frame = cv.GaussianBlur(frame, (0,0), sigmaX = 12)
    oneMinusFlow = 1 - flowThreeCanal

    blurredMap = np.multiply(blur_frame, oneMinusFlow)
    '''
    if (np.amin(blurredMap) == 0) and (np.amax(blurredMap) == 0):
         blurredMap = blurredMap
    else:
        blurredMap = (blurredMap - np.amin(blurredMap)) / (np.amax(blurredMap) - np.amin(blurredMap))
    ''' 

    finalImage = unblurredMap + blurredMap
    video_number = iFrame+1
    save_name='/home/tristan/CoursUniv/M1ALMA/TER/adaptiveBlurVideos/car13/adaptiveBlur_frame_%d.jpg' % video_number
    cv.imwrite(save_name, finalImage)

    if (np.amin(finalImage) == 0) and (np.amax(finalImage) == 0):
         finalImage = finalImage
    else:
        finalImage = (finalImage - np.amin(finalImage)) / (np.amax(finalImage) - np.amin(finalImage))
    #print(np.amin(blurredMap))
    

    #print(finalImage)
    #Permet d'afficher l'image à partir d'une matrice à un seul canal
    cv.imshow('Feur', finalImage)
    cv.waitKey(30)
    iFrame = iFrame+1
cv.destroyAllWindows()