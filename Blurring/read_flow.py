import numpy as np
import cv2 as cv

from matplotlib import pyplot as plt


frame1 = np.load('data/farneback_output/out_frame_1.npy')

cv.imshow('frame1', frame1)