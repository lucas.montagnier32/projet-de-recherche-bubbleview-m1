import cv2
import numpy as np
# create VideoCapture object
cap = cv2.VideoCapture('korea-7506.mp4')
if (cap.isOpened() == False):
    print('Error while trying to open video. Plese check again...')
# get the frame width and height
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
# define codec and create VideoWriter object
out = cv2.VideoWriter('out_videos/korea-7506_out.mp4', cv2.VideoWriter_fourcc(*'MP4V'), 30, (frame_width, frame_height))
# read until end of video
Iframe = 0
while(cap.isOpened()):
    Iframe += 1
    # capture each frame of the video
    ret, frame = cap.read()
    if ret == True:
        # add gaussian blurring to frame
        frame = cv2.GaussianBlur(frame, (0,0), sigmaX = 12)
        # save video frame
        save_path = 'blurFrames/blur_frame_%d.npy' % Iframe
        np.save(save_path, frame) 
        # display frame
        cv2.imshow('Video', frame)
        # press `q` to exit
        if cv2.waitKey(27) & 0xFF == ord('q'):
            break
    # if no frame found
    else:
        break
# release VideoCapture()
cap.release()
# close all frames and video windows
cv2.destroyAllWindows()