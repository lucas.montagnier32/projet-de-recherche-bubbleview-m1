extends Control

var group = 1
var participant_number = 0

var debug_mode = true

const const_random_id_array:Array = [6032, 3672, 5262, 9640, 5151, 6881, 8017, 3838, 1575, 6581, 3661, 8617, 4709, 9206, 6228, 1901, 1430, 8528, 6960, 6677, 4543, 7634, 7496, 7604, 8754, 8601, 7999, 2671, 2563, 8927, 4786, 1907, 4897, 3184, 8461, 4056, 2091, 2863, 6942, 5995, 4324, 9480, 2316, 1255, 9098, 2284, 9084, 7141, 6392, 9665, 3119, 6850, 2751, 1566, 3448, 5994, 9488, 6544, 8114, 7218]
const video_names:Array = [
	"Basketball","Girl1","Girl2","ManRunning1","ManRunning2","Soccer1","Soccer2","StreetBasketball1","Walking",
	"bike2","bike3","building1","building2","building3","building4",
	"car11","car12","car13","car15","person22","truck2",'truck3'
]
const video_index_normal_blur_gr1:Array = [1, 3, 5, 8, 9, 12, 13, 14, 16, 18, 21]
const video_index_normal_blur_gr2:Array = [0, 2, 4, 6, 7, 10, 11, 15, 17, 19, 20]
var index_video_list:Array = range(0,video_names.size())

onready var shader:ShaderMaterial = $VideoFlou/VideoPlayer2.material

var old_timestamp
var is_video_playing:bool = false
var file = File.new()
var random_id_array:Array
var chosen_id:int
var screen_size = Vector2(0,0)
var scale_circle = 0.33
var explanations_finished = false
var current_index_video = 0
var end_of_expe = false
var mil

var is_example_played = false

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	chosen_id = const_random_id_array[participant_number]
	screen_size.x = get_viewport().get_visible_rect().size.x # Get Width
	screen_size.y = get_viewport().get_visible_rect().size.y # Get Height
	mil = screen_size/2
	$CenterCircle.visible = false
	$CenterCircle.position = screen_size/2
	$CenterCircle.scale = Vector2(scale_circle, scale_circle)
	$Video.visible = false
	$VideoFlou.visible = false
	$Video/VideoPlayer.paused = true
	$VideoFlou/VideoPlayer2.paused = true
	$EndLabel.visible = false
	$ExplanatoryPanel/ImageExample.position = mil
	if debug_mode:
		$TestFlouLabel.visible = true
	else:
		$TestFlouLabel.visible = false
	
	file.open("res://result/result_"+str(chosen_id)+"_gr"+str(group)+".csv", File.WRITE)
	file.store_csv_line(["timestamp", "mouseX", "mouseY", "videoName"])
	# sélection des vidéos
	index_video_list.shuffle() # mélange de l'ordre des vidéos
	print(index_video_list)
	load_video()


# code utilisé juste pour créer un tableau de 60 valeurs alétoires sans doublons
func generate_random_id_array():
	var random = RandomNumberGenerator.new()
	random.randomize()
	var new_random:int
	while random_id_array.size()<60:
		new_random = random.randi_range(1000, 9999)
		if !random_id_array.has(new_random):
			random_id_array.push_back(new_random)
	print(random_id_array)


func generate_random_index_list_for_videos(number_of_videos) -> Array:
	var intlist = range(1,number_of_videos)
	intlist.shuffle()
	print(intlist)
	return intlist


func play_video():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	$Timer.stop()
	old_timestamp = OS.get_system_time_msecs()
	$Video.visible = true
	$VideoFlou.visible = true
	$Video/VideoPlayer.paused = false
	$VideoFlou/VideoPlayer2.paused = false
	$CenterCircle.visible = false
	$Video/VideoPlayer.play()
	$VideoFlou/VideoPlayer2.play()
	is_video_playing = true


func stop_video_and_show_circle():
	$CenterCircle.visible = true
	$Video.visible = false
	$VideoFlou.visible = false
	$Video/VideoPlayer.paused = true
	$VideoFlou/VideoPlayer2.paused = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if is_video_playing:
		shader.set_shader_param("mouse_position", get_global_mouse_position())
	else:
		if (mil-get_global_mouse_position()).length() < 10:
			if $Timer.is_stopped():
				$Timer.wait_time = 2
				$Timer.start()
			$CenterCircle.modulate = Color(0.7,1,0.7)
		else:
			$CenterCircle.modulate = Color(1,1,1)
			$Timer.stop()
		if Input.is_action_pressed("ui_right") && !explanations_finished:
			explanations_finished = true
			$CenterCircle.visible = true
			$ExplanatoryPanel.visible = false
		elif Input.is_action_pressed("ui_left") && explanations_finished:
			explanations_finished = false
			$CenterCircle.visible = false
			$ExplanatoryPanel.visible = true
	if Input.is_action_just_pressed("ui_cancel"): # ferme la page
		file.close()
		get_tree().quit()


# fonction appelé à chaque évenement
func _input(_event):
	if is_video_playing && is_example_played:
		file.store_csv_line([str(OS.get_system_time_msecs()-old_timestamp), str(get_global_mouse_position().x), str(get_global_mouse_position().y), video_names[index_video_list[current_index_video]]])


func _on_VideoPlayer_finished():
	is_video_playing = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if is_example_played:
		current_index_video += 1
	else:
		is_example_played = true
	if current_index_video < video_names.size():
		stop_video_and_show_circle()
		load_video()
	else: # fin de l'experience
		$EndLabel.visible = true
		$Video.visible = false
		$VideoFlou.visible = false
		$CenterCircle.visible = false
		end_of_expe = true


func load_video():
	if is_example_played:
		$Video/VideoPlayer.stream = load("res://data/videos/"+video_names[index_video_list[current_index_video]]+".ogv")
		#load de la vidéo flou ou flou adaptatif
		if group==1:
			if video_index_normal_blur_gr1.has(index_video_list[current_index_video]):
				$VideoFlou/VideoPlayer2.stream = load("res://data/videos/gaussianBlurVideos/blur_"+video_names[index_video_list[current_index_video]]+".ogv")
				$TestFlouLabel.text = "FLOU NORMAL: "+str(video_names[index_video_list[current_index_video]])
			else:
				$VideoFlou/VideoPlayer2.stream = load("res://data/videos/VideoFlouAdaptatif/adaptiveBlur_"+video_names[index_video_list[current_index_video]]+".ogv")
				$TestFlouLabel.text = "FLOU ADAPTATIF: "+str(video_names[index_video_list[current_index_video]])
		else: # group 2
			if video_index_normal_blur_gr2.has(index_video_list[current_index_video]):
				$VideoFlou/VideoPlayer2.stream = load("res://data/videos/VideoFlouAdaptatif/adaptiveBlur_"+video_names[index_video_list[current_index_video]]+".ogv")
				$TestFlouLabel.text = "FLOU ADAPTATIF: "+str(video_names[index_video_list[current_index_video]])
			else:
				$VideoFlou/VideoPlayer2.stream = load("res://data/videos/gaussianBlurVideos/blur_"+video_names[index_video_list[current_index_video]]+".ogv")
				$TestFlouLabel.text = "FLOU NORMAL: "+str(video_names[index_video_list[current_index_video]])
	else:
		$Video/VideoPlayer.stream = load("res://data/train.ogv")
		$VideoFlou/VideoPlayer2.stream = load("res://data/blur_train.ogv")
		$TestFlouLabel.text = "EXEMPLE"


# lancement de la vidéo suivante si souris au milieu pendant 2 secondes
func _on_Timer_timeout():
	if !end_of_expe:
		play_video()

# convertir les images jpg en vidéo = FFmpeg
# https://www.lemondedustopmotion.fr/news/voir/7/Transformer_une_squence_dimages_en_une_vido_avec_ffmpeg
# ffmpeg -f image2 -i %05d.jpg -r 30 -b 100000k ~/Documents/Godot/BubbleView/data/videos/Basketball.ogv

# https://makeyourgame.fun/tutoriel/jeux-videos/godot-engine/godot-engine-reveler-contenu-avec-loupe-shader
# shader language: 
# https://docs.godotengine.org/en/stable/tutorials/shaders/screen-reading_shaders.html
# https://docs.godotengine.org/en/3.0/tutorials/shading/shading_language.html

#ok gris 127 au début, demander un id (générer une liste d'id aléatoire et en les prenant 1 par 1 (pas de répétition) pour la personne (et le mettre dans le nom du csv résultat)
#ok puis panneaux explicatif avec déroulement + exemple (bien dire qu'il peuvent arreter quand ils veulent et que leurs données peuvent être supprimer)
#ok la personne doit mettre la souris dans un cercle au milieu entre chaque vidéos pour qu'on puisse lancer la vidéo
#ok entre chaque vidéo écran gris 127 vide puis enchaine les vidéo
# gris puis vidéo exemple puis gris puis vrai vidéos dans un ordre aléatoire
#ok message de fin (merci de votre participation)
#ok pas besoin de récupérer la position de la souris entre les vidéos
#ok mettre en plein écran
#ok faire un fichier csv résultat par participant (mettre les noms des vidéos dans le fichier)

# faire une mélange entre bv classique et bv flou adaptatif 50/50 ?
# 2 groupes de personnes = 10 en flou normale puis 10 en flou adaptatif (mélangé) et l'autre groupe c'est l'inverse (numéro de groupe a rajouter dans le questionnaire papier au début)
# 30 personnes par groupe = 60 personnes
