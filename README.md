#### Dossier Analyse Python:
- saliency_frame.py contient des fonctions pour créer un graphique interactif des données d'attention sur une vidéo en fonction du temps (carte de saillance)
- saliency_map.py sert a créer une carte d'attention sur toute la durée d'une vidéo (scanpath)

#### Dossier BubbleView Godot:
- fichiers godot engine pour des différentes méthodes d'implémentation de la BubbleView

#### Contributeurs
- Lucas Montagnier
- Tristan Ramé
